class Prestador {
  int prestadorId;
  bool ativo;
  int userId;
  DateTime createdAt;
  DateTime updatedAt;

  Prestador(
      {this.prestadorId,
      this.ativo,
      this.userId,
      this.createdAt,
      this.updatedAt});

  factory Prestador.fromJson(Map<String, dynamic> json) {
    return new Prestador(
      prestadorId: json['prestador_id'],
      ativo: json['name'],
      userId: json['user_id'],
      createdAt: DateTime.parse(json['created_at']),
      updatedAt: DateTime.parse(json['updated_at']),
    );
  }

  Map<String, dynamic> toJson() => {
        'prestadorId': prestadorId,
        'ativo': ativo,
        'userId': userId,
        'createdAt': createdAt,
        'updatedAt': updatedAt
      };
}
