import 'package:app_suporte/Models/Item.dart';

class Interesse {
  int interessePrestadorId;
  int prestadorId;
  int postId;
  double preco;
  String descricao;
  String namePrestador;
  String descriptionToUser;
  List<Item> optionInteresse;
  DateTime createdAt;
  DateTime updatedAt;

  Interesse(
      {this.descricao,
      this.postId,
      this.createdAt,
      this.updatedAt,
      this.interessePrestadorId,
      this.preco,
      this.prestadorId,
      this.descriptionToUser,
      this.optionInteresse,
      this.namePrestador});

  factory Interesse.fromJson(Map<String, dynamic> json) {
    return new Interesse(
      postId: json['post_id'],
      descricao: json["descricao"] == null ? '' : json['descricao'],
      interessePrestadorId: json['interesse_prestador_id'],
      preco:
          json['preco'] != null ? double.parse(json['preco'].toString()) : null,
      prestadorId: json['prestador_id'],
      namePrestador: json['name_prestador'],
      createdAt: DateTime.parse(json['created_at']),
      updatedAt: DateTime.parse(json['updated_at']),
    );
  }
}
