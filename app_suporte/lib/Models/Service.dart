class Service {
  int servicoId;
  bool checked;
  String name;
  String descricao;
  DateTime createdAt;
  DateTime updatedAt;

  Service(
      {this.servicoId,
      this.name,
      this.descricao,
      this.checked,
      this.updatedAt,
      this.createdAt});

  factory Service.fromJson(Map<String, dynamic> json) {
    return new Service(
      servicoId: json['servico_id'],
      name: json['nome'],
      descricao: json['descricao'],
      checked: json['checked'],
      createdAt: DateTime.parse(json['created_at']),
      updatedAt: DateTime.parse(json['updated_at']),
    );
  }

  Map<String, dynamic> toJson() => {
        'serviceId': servicoId,
        'name': name,
        'descricao': descricao,
        'checked': checked,
        'createdAt': createdAt,
        'updatedAt': updatedAt
      };
}
