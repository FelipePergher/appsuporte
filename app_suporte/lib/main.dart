import 'package:app_suporte/Controller/AuthController.dart';
import 'package:app_suporte/Views/posts.dart';
import 'package:app_suporte/Views/reset_password.dart';
import 'package:flutter/material.dart';
import 'package:app_suporte/Views/register.dart';

void main() => runApp(new Suporte());

class Suporte extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      title: 'SuporTech',
      routes: <String, WidgetBuilder>{
        '/login': (BuildContext context) => new HomePage(),
        '/home': (BuildContext context) => new Posts(),
      },
      theme: new ThemeData(
        primarySwatch: Colors.brown,
        brightness: Brightness.light,
        buttonColor: Colors.brown,
        primaryColor: Colors.brown,
      ),
      home: new HomePage(title: 'Login'),
      debugShowCheckedModeBanner: false,
    );
  }
}

class HomePage extends StatefulWidget {
  HomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _HomePageState createState() => new _HomePageState();
}

class _HomePageState extends State<HomePage> {
  BuildContext _scaffoldContext;
  AuthController authController = new AuthController();
  final formKey = new GlobalKey<FormState>();
  String _email;
  String _password;

  void _submit() {
    final form = formKey.currentState;

    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (_) => new Container(
            margin: const EdgeInsets.only(
                top: 200.0, bottom: 200.0, left: 50.0, right: 50.0),
            alignment: AlignmentDirectional.center,
            decoration: new BoxDecoration(
                color: Colors.brown[100],
                borderRadius: new BorderRadius.circular(10.0)),
            child: new Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                new Center(
                  child: new SizedBox(
                    height: 50.0,
                    width: 50.0,
                    child: new CircularProgressIndicator(strokeWidth: 7.0),
                  ),
                ),
                new Container(
                    margin: const EdgeInsets.only(top: 20.0),
                    child: new Text(
                      'Aguarde...',
                      style: new TextStyle(
                          color: Colors.black,
                          fontSize: 18.0,
                          decoration: TextDecoration.none),
                    )),
              ],
            ),
          ),
    );

    if (form.validate()) {
      form.save();

      authController.login(_email, _password).then((bool value) {
        if (value) {
          Navigator.pop(context);
          Navigator.pushAndRemoveUntil(
              context,
              new MaterialPageRoute(builder: (context) => new Posts()),
              ModalRoute.withName("/login"));
        } else {
          Navigator.pop(context);
          showErrorMessage('Usuário ou senha incorreta');
        }
      }, onError: (e) {
        Navigator.pop(context);
        showErrorMessage('Algo errado aconteceu!');
      });
    }
  }

  void showErrorMessage(String message) {
    Scaffold.of(_scaffoldContext).showSnackBar(new SnackBar(
          content: new Text(message),
          duration: new Duration(seconds: 5),
        ));
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        body: new Form(
      key: formKey,
      child: new ListView(
        children: <Widget>[
          new Container(
            margin: const EdgeInsets.only(top: 70.0),
            child: new Image.asset(
              'images/logo.png',
              height: 100.0,
            ),
          ),
          new Container(
            margin: const EdgeInsets.only(top: 40.0),
            child: new ListTile(
              leading: const Icon(Icons.mail),
              title: new TextFormField(
                keyboardType: TextInputType.emailAddress,
                decoration: new InputDecoration(
                  hintText: "E-mail",
                ),
                validator: (String value) {
                  if (value.isEmpty) return 'E-mail é obrigatório';
                  String p = "[a-zA-Z0-9\+\.\_\%\-\+]{1,256}" +
                      "\\@ [a-zA-Z0-9][a-zA-Z0-9\\-]{0,64}" +
                      "(\\.[a-zA-Z0-9][a-zA-Z0-9\\-]{0,25})+";
                  RegExp regExp = new RegExp(p);
                  if (regExp.hasMatch(value)) {
                    return 'Email invalido';
                  }
                  return null;
                },
                onSaved: (val) => _email = val,
              ),
            ),
          ),
          new ListTile(
            leading: const Icon(Icons.lock_outline),
            title: new TextFormField(
              obscureText: true,
              keyboardType: TextInputType.text,
              decoration: new InputDecoration(
                hintText: "Senha",
              ),
              validator: (val) => val.length < 6 ? 'senha curta' : null,
              onSaved: (val) => _password = val,
            ),
          ),
          new Container(
            margin: const EdgeInsets.only(top: 20.0),
            alignment: new Alignment(0.0, 0.0),
            child: new RaisedButton(
              child: new Text(
                'Entrar',
                style: new TextStyle(
                  fontSize: 18.0,
                  color: Colors.white,
                  fontWeight: FontWeight.bold,
                ),
              ),
              color: Colors.brown,
              onPressed: () {
                _submit();
                /*Navigator.push(
                      context,
                      new MaterialPageRoute(builder: (context) => new Posts()),
                    );*/
              },
            ),
          ),
          new Container(
            margin: const EdgeInsets.only(top: 40.0),
            alignment: new Alignment(0.0, 0.0),
            child: new InkWell(
              child: new Text(
                'Esqueci minha senha',
                style: new TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 18.0,
                  color: Colors.brown,
                ),
              ),
              onTap: () {
                Navigator.push(
                  context,
                  new MaterialPageRoute(
                      builder: (context) => new ResetPassword()),
                );
              },
            ),
          ),
          new Container(
              margin: const EdgeInsets.only(top: 20.0),
              alignment: new Alignment(0.0, 0.0),
              child: new Row(
                mainAxisAlignment: MainAxisAlignment.center,
                mainAxisSize: MainAxisSize.max,
                children: <Widget>[
                  new Text(
                    'Novo por aqui?',
                    style: new TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 18.0,
                    ),
                  ),
                  new InkWell(
                    child: new Text(
                      ' Faça seu cadastro!',
                      style: new TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 18.0,
                        color: Colors.brown,
                      ),
                    ),
                    onTap: () {
                      Navigator.push(
                        context,
                        new MaterialPageRoute(
                            builder: (context) => new Register()),
                      );
                    },
                  ),
                ],
              )),
          new Builder(builder: (BuildContext context) {
            _scaffoldContext = context;
            return new Center();
          }),
        ],
      ),
    ));
  }
}
