import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:http/http.dart' as http;

import 'package:app_suporte/Code/global.dart';
import 'package:app_suporte/Models/User.dart';

class UserController {
  Future<User> getUserById(int id) async {
    final response = await http.get(
      Global.url + '/api/user/' + id.toString(),
      headers: {HttpHeaders.AUTHORIZATION: "Bearer " + Global.token},
    );
    final responseJson = json.decode(response.body);
    return new User.fromJson(responseJson["data"]);
  }

  Future<User> getUserByPrestadorId(int id) async {
    final response = await http.get(
      Global.url + '/api/getUserByPrestadorId/' + id.toString(),
      headers: {HttpHeaders.AUTHORIZATION: "Bearer " + Global.token},
    );
    final responseJson = json.decode(response.body);
    return new User.fromJson(responseJson["data"]);
  }
}
