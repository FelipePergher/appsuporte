import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:app_suporte/Code/global.dart';
import 'package:http/http.dart' as http;

import 'package:app_suporte/Models/User.dart';

class AuthController {
  Future<bool> login(String name, String password) async {
    final response = await http.post(
      Global.url + '/oauth/token',
      body: {
        "username": name,
        "password": password,
        "grant_type": "password",
        "client_id": "2",
        "client_secret": "0BMlOPpdpS52EaH7CNILdPCWlATBe8i0IlXwd6Qu"
      },
    );

    final responseJson = json.decode(response.body);
    Global.token = responseJson["access_token"];

    if (Global.token != null && Global.token != "") {
      Global.username = name;
      final response1 = await http.get(Global.url + '/api/prestador/' + name,
          headers: {HttpHeaders.AUTHORIZATION: "Bearer " + Global.token});

      final responseJson1 = json.decode(response1.body);
      if (responseJson1) {
        Global.prestador = true;
      } else {
        Global.prestador = false;
      }

      return true;
    } else {
      return false;
    }
  }

  Future<String> register(User user) async {
    final response = await http.post(
      Global.url + '/api/user',
      headers: {
        'Content-type': 'application/json',
        'Accept': 'application/json'
      },
      body: jsonEncode(user),
    );

    final responseJson = json.decode(response.body);
    if (responseJson["email"] != null && responseJson["cpf"] != null) {
      return "Email e cpf já cadastrado!";
    } else if (responseJson["email"] != null) {
      return "Email já cadastrado!";
    } else if (responseJson["cpf"] != null) {
      return "CPF já cadastrado!";
    } else {
      User createdUser = new User.fromJson(responseJson["data"]);
      await login(createdUser.email, user.password).then((bool value) {
        if (value) {
          return "success";
        } else {
          return "Algo errado aconteceu!";
        }
      }, onError: (e) {
        return "Algo errado aconteceu!";
      });
      return "success";
    }
  }
}
