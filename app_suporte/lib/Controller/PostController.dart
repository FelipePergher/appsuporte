import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:app_suporte/Code/global.dart';
import 'package:app_suporte/Models/Post.dart';
import 'package:app_suporte/ViewModel/PostViewModel.dart';
import 'package:http/http.dart' as http;

class PostController {
  Future<bool> addPost(PostViewModel postViewModel) async {
    DateTime now = DateTime.now();
    String horaI = new DateTime(now.year, now.month, now.day,
            postViewModel.timeInitial.hour, postViewModel.timeInitial.minute)
        .toIso8601String();

    String horaF = new DateTime(now.year, now.month, now.day,
            postViewModel.timeFinal.hour, postViewModel.timeFinal.minute)
        .toIso8601String();

    Post _post = new Post(
        servicoId: postViewModel.service.servicoId,
        descricao: postViewModel.descricao,
        horaInitial: horaI,
        horaFinal: horaF,
        diaSemana: "");
    for (var day in postViewModel.weekDays) {
      if (day.checked) {
        _post.diaSemana += day.name + "||";
      }
    }

    final response = await http.post(
      Global.url + '/api/post',
      headers: {
        HttpHeaders.AUTHORIZATION: "Bearer " + Global.token,
        'Content-type': 'application/json',
        'Accept': 'application/json'
      },
      body: jsonEncode({'post': _post, 'username': Global.username}),
    );

    final responseJson = json.decode(response.body);

    if (responseJson) {
      return true;
    } else {
      return false;
    }
  }

  Future<List<Post>> getPosts() async {
    final response = await http.get(
      Global.url + '/api/getPosts/' + Global.username,
      headers: {HttpHeaders.AUTHORIZATION: "Bearer " + Global.token},
    );

    final responseJson = json.decode(response.body);
    List<Post> posts = new List();

    if (responseJson["data"] != null) {
      for (var item in responseJson["data"]) {
        Post service = Post.fromJson(item);
        posts.add(service);
      }
    }

    return posts;
  }

  Future<List<Post>> getMyPosts() async {
    final response = await http.get(
      Global.url + '/api/getMyPosts/' + Global.username,
      headers: {HttpHeaders.AUTHORIZATION: "Bearer " + Global.token},
    );

    final responseJson = json.decode(response.body);
    List<Post> posts = new List();

    if (responseJson["data"] != null) {
      for (var item in responseJson["data"]) {
        Post service = Post.fromJson(item);
        posts.add(service);
      }
    }

    return posts;
  }

  Future<bool> removePost(int postId) async {
    final response = await http
        .get(Global.url + '/api/post/destroy/' + postId.toString(), headers: {
      HttpHeaders.AUTHORIZATION: "Bearer " + Global.token,
    });

    final responseJson = json.decode(response.body);

    if (responseJson) {
      return true;
    } else {
      return false;
    }
  }

  Future<bool> interessePost(int postId, String message, String price) async {
    final response = await http.post(
      Global.url + '/api/interesse',
      headers: {
        HttpHeaders.AUTHORIZATION: "Bearer " + Global.token,
        'Content-type': 'application/json',
        'Accept': 'application/json'
      },
      body: jsonEncode({
        'postId': postId,
        'username': Global.username,
        'message': message,
        'price': price
      }),
    );

    final responseJson = json.decode(response.body);

    if (responseJson) {
      return true;
    } else {
      return false;
    }
  }

  Future<bool> desinteressePost(int postId) async {
    final response = await http.post(
      Global.url + '/api/desinteresse',
      headers: {
        HttpHeaders.AUTHORIZATION: "Bearer " + Global.token,
        'Content-type': 'application/json',
        'Accept': 'application/json'
      },
      body: jsonEncode({'postId': postId, 'username': Global.username}),
    );

    final responseJson = json.decode(response.body);

    if (responseJson) {
      return true;
    } else {
      return false;
    }
  }
}
