import 'package:app_suporte/Controller/PostController.dart';
import 'package:app_suporte/Controller/PrestadorController.dart';
import 'package:app_suporte/Controller/ServiceController.dart';
import 'package:app_suporte/Controller/UserController.dart';
import 'package:app_suporte/Models/Item.dart';
import 'package:app_suporte/Models/Post.dart';
import 'package:app_suporte/Models/Interesse.dart';
import 'package:app_suporte/Models/User.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class MyPosts extends StatefulWidget {
  @override
  _MyPosts createState() => new _MyPosts();
}

class _MyPosts extends State<MyPosts> {
  PostController postController = new PostController();
  UserController userController = new UserController();
  List<Post> posts;
  BuildContext _scaffoldContext;

  void _handleAction(Item item) {
    if (item.id == '1') {
      if (item.post.interessePrestador.length > 0) {
        for (var item in item.post.interessePrestador) {
          if (item.preco != null && item.preco > 0) {
            item.descriptionToUser = item.namePrestador +
                "  (R\$ " +
                item.preco.toStringAsFixed(2) +
                ")";
          } else {
            item.descriptionToUser = item.namePrestador;
          }
        }

        _popupInteresses(item.post);
      }
    } else if (item.id == '2') {
      showDialog(
        context: context,
        barrierDismissible: false,
        builder: (_) => new Center(child: new CircularProgressIndicator()),
      );

      postController.removePost(item.post.postId).then((bool result) {
        if (result) {
          Navigator.pop(context);
          setState(() {
            posts.removeWhere((post) => post.postId == item.post.postId);
          });
        } else {
          Navigator.pop(context);
          showErrorMessage('Algo errado aconteceu!');
        }
      }, onError: (e) {
        Navigator.pop(context);
        showErrorMessage('Algo errado aconteceu!');
      });
    }
  }

  void showErrorMessage(String message) {
    Scaffold.of(_scaffoldContext).showSnackBar(new SnackBar(
          content: new Text(message),
          duration: new Duration(seconds: 5),
        ));
  }

  void _popupInteresses(Post post) {
    showDialog(
      context: context,
      builder: (_scaffoldContext) => new PopupInteresse(
            post: post,
            updateIcon: _updateIcon,
            removeButtonInteresse: _removeButtonInteresse,
          ),
    );
  }

  void _infoInteresse(Post post) {
    showDialog(
        context: context,
        builder: (_) => new SimpleDialog(
              title: new Text(
                "Informações do Post",
                textAlign: TextAlign.center,
              ),
              children: <Widget>[
                new SimpleDialogOption(
                  child: Text('Serviço: ' + post.serviceName),
                ),
                new SimpleDialogOption(
                  child: Text('Descrição: ' + post.descricao),
                ),
                new SimpleDialogOption(
                  child: Text(
                      'Horário Inicial: ' + post.horaInitial.substring(0, 5)),
                ),
                new SimpleDialogOption(
                  child:
                      Text('Horário Final: ' + post.horaFinal.substring(0, 5)),
                ),
                new SimpleDialogOption(
                  child: Text(
                      'Dia da Semana: ' + post.diaSemana.replaceAll('||', ',')),
                ),
                new Container(
                  margin:
                      const EdgeInsets.only(left: 20.0, right: 20.0, top: 10.0),
                  child: RaisedButton(
                    onPressed: () {
                      Navigator.pop(context);
                    },
                    child: Text(
                      "Ok",
                      style: new TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 18.0,
                        color: Colors.white,
                      ),
                    ),
                  ),
                ),
              ],
            ));
  }

  void _updateIcon(Item item) {
    setState(() {
      posts.firstWhere((pos) => pos.postId == item.post.postId).icon =
          new Icon(Icons.sentiment_very_satisfied, color: Colors.black);
      posts.firstWhere((pos) => pos.postId == item.post.postId).color =
          Colors.green;

      posts
          .firstWhere((pos) => pos.postId == item.post.postId)
          .listItem
          .removeAt(0);
    });
  }

  void _removeButtonInteresse(Item item) {
    setState(() {
      posts.firstWhere((pos) => pos.postId == item.post.postId).icon =
          new Icon(Icons.sentiment_neutral, color: Colors.black);
      posts.firstWhere((pos) => pos.postId == item.post.postId).color =
          Colors.orange;

      posts
          .firstWhere((pos) => pos.postId == item.post.postId)
          .listItem[0]
          .enabled = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text("Minhas Publicações"),
      ),
      body: new FutureBuilder<List<Post>>(
        future: postController.getMyPosts(),
        builder: (context, snapshot) {
          _scaffoldContext = context;
          if (snapshot.hasData) {
            if (posts == null) {
              posts = snapshot.data;

              if (posts.length == 0) {
                return new Center(
                  child: new Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      new Container(
                        margin: const EdgeInsets.only(bottom: 20.0),
                        child:
                            new Icon(Icons.sentiment_dissatisfied, size: 60.0),
                      ),
                      new Text(
                        'Você ainda não publicou nada!',
                        style: new TextStyle(fontSize: 20.0),
                      )
                    ],
                  ),
                );
              }

              for (var post in posts) {
                if (post.concluido != null && post.concluido) {
                  post.listItem = <Item>[
                    Item(
                        id: "2",
                        option: 'Apagar post',
                        post: post,
                        enabled: true),
                  ];

                  post.color = Colors.green;
                  post.icon = new Icon(Icons.sentiment_very_satisfied,
                      color: Colors.black);
                } else if (post.interessePrestador.length > 0) {
                  post.listItem = <Item>[
                    Item(
                        id: "1",
                        option: 'Interesses',
                        post: post,
                        enabled: true),
                    Item(
                        id: "2",
                        option: 'Apagar post',
                        post: post,
                        enabled: true),
                  ];

                  for (var interesse in post.interessePrestador) {
                    interesse.optionInteresse = <Item>[
                      Item(
                          option: 'Feito',
                          id: '1',
                          post: post,
                          enabled: true,
                          interesse: interesse),
                      Item(
                          option: 'Remover Interesse',
                          id: '2',
                          post: post,
                          enabled: true,
                          interesse: interesse)
                    ];
                  }

                  post.color = Colors.blue;
                  post.icon =
                      new Icon(Icons.sentiment_satisfied, color: Colors.black);
                } else {
                  post.listItem = <Item>[
                    Item(
                        id: "1",
                        option: 'Interesses',
                        post: post,
                        enabled: false),
                    Item(
                        id: "2",
                        option: 'Apagar post',
                        post: post,
                        enabled: true),
                  ];

                  post.color = Colors.orange;
                  post.icon =
                      new Icon(Icons.sentiment_neutral, color: Colors.black);
                }
              }
            }

            if (posts.length == 0) {
              return new Center(
                child: new Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    new Container(
                      margin: const EdgeInsets.only(bottom: 20.0),
                      child: new Icon(Icons.sentiment_dissatisfied, size: 60.0),
                    ),
                    new Text(
                      'Você ainda não publicou nada!',
                      style: new TextStyle(fontSize: 20.0),
                    )
                  ],
                ),
              );
            }

            return new ListView.builder(
              itemBuilder: (BuildContext context, int index) => new Column(
                    children: <Widget>[
                      new ListTile(
                          trailing: new PopupMenuButton(
                            onSelected: _handleAction,
                            itemBuilder: (BuildContext context) {
                              return posts[index].listItem.map((Item option) {
                                return new PopupMenuItem<Item>(
                                  value: option,
                                  enabled: option.enabled,
                                  child: new Text(option.option),
                                );
                              }).toList();
                            },
                          ),
                          leading: new CircleAvatar(
                            backgroundColor: posts[index].color,
                            child: posts[index].icon,
                          ),
                          title: new Text(posts[index].serviceName),
                          subtitle: new Text(posts[index].descricao),
                          onTap: () {
                            _infoInteresse(posts[index]);
                          }),
                      new Divider(color: Colors.brown),
                    ],
                  ),
              itemCount: posts.length,
            );
          } else if (snapshot.hasError) {
            return new Text("${snapshot.error}");
          }
          return new Center(
            child: new CircularProgressIndicator(),
          );
        },
      ),
    );
  }
}

class PopupInteresse extends StatefulWidget {
  const PopupInteresse(
      {this.post, this.updateIcon, this.removeButtonInteresse});

  final Post post;
  final void Function(Item) updateIcon;
  final void Function(Item) removeButtonInteresse;

  @override
  State createState() => new _PopupInteresse();
}

class _PopupInteresse extends State<PopupInteresse> {
  PrestadorController prestadorController = new PrestadorController();
  UserController userController = new UserController();
  ServiceController serviceController = new ServiceController();
  BuildContext _scaffoldContext;
  Post post;

  @override
  void initState() {
    super.initState();
    post = widget.post;
  }

  void _donePost(Item item) {
    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (_) => new Center(child: new CircularProgressIndicator()),
    );

    serviceController
        .saveServicePrestado(item.post.postId, item.interesse.prestadorId)
        .then((bool result) {
      Navigator.pop(context);
      if (result) {
        widget.updateIcon(item);

        Navigator.pop(context);
      } else {
        showErrorMessage('Algo errado aconteceu!');
      }
    }, onError: (e) {
      showErrorMessage('Algo errado aconteceu!');
    });
    new Center(
      child: new CircularProgressIndicator(),
    );
  }

  void showErrorMessage(String message) {
    Scaffold.of(_scaffoldContext).showSnackBar(new SnackBar(
          content: new Text(message),
          duration: new Duration(seconds: 5),
        ));
  }

  void _infoInteresse(Interesse interesse) {
    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (_) => new Center(child: new CircularProgressIndicator()),
    );

    userController.getUserByPrestadorId(interesse.prestadorId).then(
        (User user) {
      Navigator.pop(context);
      showDialog(
          context: context,
          builder: (_) => new SimpleDialog(
                title: new Text("Informações do solicitante",
                    textAlign: TextAlign.center),
                children: <Widget>[
                  new SimpleDialogOption(
                    child: Text('Nome: ' + user.name),
                  ),
                  new SimpleDialogOption(
                    child: Text('Email: ' + user.email),
                  ),
                  new SimpleDialogOption(
                      child: Text(
                          'Telefone: ' + user.telefone + ' / ' + user.celular)),
                  new SimpleDialogOption(
                      child: Text('Descrição: ' + interesse.descricao)),
                  new SimpleDialogOption(
                      child: Text('Preço: R\$ ' +
                          (interesse.preco != null
                              ? interesse.preco.toStringAsFixed(2)
                              : ''))),
                  new Container(
                    margin: const EdgeInsets.only(
                        left: 20.0, right: 20.0, top: 10.0),
                    child: RaisedButton(
                      onPressed: () {
                        Navigator.pop(context);
                      },
                      child: Text(
                        "Ok",
                        style: new TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 18.0,
                          color: Colors.white,
                        ),
                      ),
                    ),
                  ),
                ],
              ));
    }, onError: (e) {
      showErrorMessage('Algo errado aconteceu!');
    });
  }

  void _handleActionInteresse(Item item) {
    if (item.id == '1') {
      _donePost(item);
    } else if (item.id == '2') {
      showDialog(
        barrierDismissible: false,
        context: context,
        builder: (_) => new Center(child: new CircularProgressIndicator()),
      );
      prestadorController
          .removeInteressePrestador(item.interesse.interessePrestadorId)
          .then((bool result) {
        Navigator.pop(context);
        if (result) {
          setState(() {
            post.interessePrestador.removeWhere((interesse) =>
                interesse.interessePrestadorId ==
                item.interesse.interessePrestadorId);
            if (post.interessePrestador.length == 0) {
              widget.removeButtonInteresse(item);
              Navigator.pop(context);
            }
          });
        } else {
          showErrorMessage('Algo errado aconteceu!');
        }
      }, onError: (e) {
        showErrorMessage('Algo errado aconteceu!');
      });
    }
  }

  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: Text('Interesses'),
        automaticallyImplyLeading: false,
        actions: <Widget>[
          new Container(
              child: new IconButton(
            icon: new Icon(Icons.close),
            onPressed: () {
              Navigator.pop(context);
            },
          )),
        ],
      ),
      body: new ListView.builder(
        scrollDirection: Axis.vertical,
        itemBuilder: (BuildContext context, int index) => new Column(
              children: <Widget>[
                new ListTile(
                  leading: new InkWell(
                    child: Icon(Icons.info),
                    onTap: () {
                      _infoInteresse(post.interessePrestador[index]);
                    },
                  ),
                  trailing: new PopupMenuButton(
                    onSelected: _handleActionInteresse,
                    itemBuilder: (BuildContext context) {
                      return post.interessePrestador[index].optionInteresse
                          .map((Item option) {
                        return new PopupMenuItem<Item>(
                          value: option,
                          enabled: option.enabled,
                          child: new Text(option.option),
                        );
                      }).toList();
                    },
                  ),
                  title: Text(post.interessePrestador[index].descriptionToUser),
                  subtitle: Text(
                    post.interessePrestador[index].descricao,
                  ),
                ),
                new Builder(builder: (BuildContext context) {
                  _scaffoldContext = context;
                  return new Center();
                }),
              ],
            ),
        itemCount: post.interessePrestador.length,
      ),
    );
  }
}
