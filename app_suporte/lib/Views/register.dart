import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:app_suporte/Views/choice.dart';
import 'package:app_suporte/Models/User.dart';
import 'package:app_suporte/Controller/AuthController.dart';
import 'package:flutter_masked_text/flutter_masked_text.dart';

import '../Code/EnsureVisibleWhenFocused.dart';

class Register extends StatefulWidget {
  @override
  _Register createState() => new _Register();
}

class _Register extends State<Register> {
  AuthController authController = new AuthController();
  BuildContext _scaffoldContext;
  User _user = new User();
  final formKey = new GlobalKey<FormState>();
  int _radioValue = 0;

  FocusNode _focusNodeName = new FocusNode();
  FocusNode _focusNodeAddress = new FocusNode();
  FocusNode _focusNodePhone = new FocusNode();
  FocusNode _focusNodeCel = new FocusNode();
  FocusNode _focusNodeCpf = new FocusNode();
  FocusNode _focusNodeMail = new FocusNode();
  FocusNode _focusNodePassword = new FocusNode();

  TextEditingController _nameController = new TextEditingController();
  TextEditingController _addressController = new TextEditingController();
  TextEditingController _mailController = new TextEditingController();
  TextEditingController _passwordController = new TextEditingController();

  MaskedTextController _phoneController =
      new MaskedTextController(mask: '(00) 0000-0000');
  MaskedTextController _celController =
      new MaskedTextController(mask: '(00) 0 0000-0000');
  MaskedTextController _cpfController =
      new MaskedTextController(mask: '000.000.000-00');

  void handleRadioValueChanged(int value) {
    setState(() {
      _radioValue = value;
    });
  }

  void _submit() {
    final form = formKey.currentState;
    if (form.validate()) {
      showDialog(
        context: context,
        barrierDismissible: false,
        builder: (_) => new Center(child: new CircularProgressIndicator()),
      );

      form.save();
      if (_radioValue == 0) {
        _user.sexo = 'F';
      } else if (_radioValue == 1) {
        _user.sexo = 'M';
      }
      authController.register(_user).then((String message) {
        if (message == "success") {
          Navigator.push(
            context,
            new MaterialPageRoute(builder: (context) => new Choice()),
          );
        } else {
          Navigator.pop(context);
          showErrorMessage(message);
        }
      }, onError: (e) {
        Navigator.pop(context);
        showErrorMessage('Algo errado aconteceu!');
      });
    }
  }

  void showErrorMessage(String message) {
    Scaffold.of(_scaffoldContext).showSnackBar(new SnackBar(
          content: new Text(message),
          duration: new Duration(seconds: 5),
        ));
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        appBar: new AppBar(
          title: new Text("Cadastro"),
        ),
        body: new Form(
          key: formKey,
          child: new ListView(
            children: <Widget>[
              new ListTile(
                leading: const Icon(Icons.person),
                title: new EnsureVisibleWhenFocused(
                  focusNode: _focusNodeName,
                  child: new TextFormField(
                    focusNode: _focusNodeName,
                    controller: _nameController,
                    keyboardType: TextInputType.text,
                    decoration: new InputDecoration(
                      hintText: "Nome Completo",
                    ),
                    validator: (String value) {
                      if (value.isEmpty) return 'Nome é obrigatório';
                      if (value.length < 6) return "Nome muito pequeno";
                      return null;
                    },
                    onSaved: (val) => _user.name = val,
                  ),
                ),
              ),
              new ListTile(
                leading: const Icon(Icons.place),
                title: new EnsureVisibleWhenFocused(
                  focusNode: _focusNodeAddress,
                  child: new TextFormField(
                    controller: _addressController,
                    focusNode: _focusNodeAddress,
                    keyboardType: TextInputType.text,
                    decoration: new InputDecoration(
                      hintText: "Endereço",
                    ),
                    validator: (String value) {
                      if (value.isEmpty) return 'Endereco é obrigatório';
                      if (value.length < 10) return "Endereço muito pequeno";
                      return null;
                    },
                    onSaved: (val) => _user.endereco = val,
                  ),
                ),
              ),
              new ListTile(
                leading: const Icon(Icons.phone_in_talk),
                title: new EnsureVisibleWhenFocused(
                  focusNode: _focusNodePhone,
                  child: new TextFormField(
                    controller: _phoneController,
                    focusNode: _focusNodePhone,
                    keyboardType: TextInputType.phone,
                    decoration: new InputDecoration(
                      hintText: "Telefone",
                    ),
                    validator: (String value) {
                      if (value.isEmpty) return 'Telefone é obrigatório';
                      if (value.length != 14)
                        return "Telefone deve conter 10 digitos";
                      return null;
                    },
                    onSaved: (val) => _user.telefone = val,
                  ),
                ),
              ),
              new ListTile(
                leading: const Icon(Icons.phone),
                title: new EnsureVisibleWhenFocused(
                  focusNode: _focusNodeCel,
                  child: new TextFormField(
                    controller: _celController,
                    focusNode: _focusNodeCel,
                    keyboardType: TextInputType.phone,
                    decoration: new InputDecoration(
                      hintText: "Celular",
                    ),
                    validator: (String value) {
                      if (value.isEmpty) return 'Celular é obrigatório';
                      if (value.length != 16)
                        return "Celular deve conter 11 digitos";
                      return null;
                    },
                    onSaved: (val) => _user.celular = val,
                  ),
                ),
              ),
              new ListTile(
                leading: const Icon(Icons.card_membership),
                title: new EnsureVisibleWhenFocused(
                  focusNode: _focusNodeCpf,
                  child: new TextFormField(
                    controller: _cpfController,
                    focusNode: _focusNodeCpf,
                    keyboardType: TextInputType.phone,
                    decoration: new InputDecoration(
                      hintText: "CPF",
                    ),
                    validator: (String value) {
                      if (value.isEmpty) return 'CPF é obrigatório';
                      if (value.length != 14)
                        return "CPF deve conter 14 digitos";
                      return null;
                    },
                    onSaved: (val) => _user.cpf = val,
                  ),
                ),
              ),
              new ListTile(
                leading: const Icon(Icons.person),
                title: new Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      new Row(
                        children: [
                          new Radio<int>(
                              value: 0,
                              groupValue: _radioValue,
                              onChanged: handleRadioValueChanged),
                          new Text('Feminino'),
                        ],
                      ),
                      new Row(
                        children: [
                          new Radio<int>(
                              value: 1,
                              groupValue: _radioValue,
                              onChanged: handleRadioValueChanged),
                          new Text('Masculino'),
                        ],
                      ),
                    ]),
              ),
              new ListTile(
                leading: const Icon(Icons.mail),
                title: new EnsureVisibleWhenFocused(
                  focusNode: _focusNodeMail,
                  child: new TextFormField(
                    focusNode: _focusNodeMail,
                    controller: _mailController,
                    keyboardType: TextInputType.emailAddress,
                    decoration: new InputDecoration(
                      hintText: "E-mail",
                    ),
                    validator: (String value) {
                      if (value.isEmpty) return 'E-mail é obrigatório';
                      String p = "[a-zA-Z0-9\+\.\_\%\-\+]{1,256}" +
                          "\\@ [a-zA-Z0-9][a-zA-Z0-9\\-]{0,64}" +
                          "(\\.[a-zA-Z0-9][a-zA-Z0-9\\-]{0,25})+";
                      RegExp regExp = new RegExp(p);
                      if (regExp.hasMatch(value)) {
                        return 'Email invalido';
                      }
                      return null;
                    },
                    onSaved: (val) => _user.email = val,
                  ),
                ),
              ),
              new ListTile(
                leading: const Icon(Icons.lock_outline),
                title: new EnsureVisibleWhenFocused(
                  focusNode: _focusNodePassword,
                  child: new TextFormField(
                    controller: _passwordController,
                    obscureText: true,
                    focusNode: _focusNodePassword,
                    keyboardType: TextInputType.text,
                    decoration: new InputDecoration(
                      hintText: "Senha",
                    ),
                    validator: (val) => val.length < 6
                        ? 'Senha deve conter mo mínimo 6 caracteres'
                        : null,
                    onSaved: (val) => _user.password = val,
                  ),
                ),
              ),
              new Container(
                margin: const EdgeInsets.only(top: 30.0),
                alignment: new Alignment(0.0, 0.0),
                child: new RaisedButton(
                  child: new Text(
                    'Salvar',
                    style: new TextStyle(
                      fontSize: 20.0,
                      color: Colors.white,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  onPressed: () {
                    _submit();
                  },
                ),
              ),
              new Builder(builder: (BuildContext context) {
                _scaffoldContext = context;
                return new Center();
              }),
            ],
          ),
        ));
  }
}
