import 'package:app_suporte/Views/posts.dart';
import 'package:app_suporte/Views/select_services.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class Choice extends StatefulWidget {
  @override
  _Choice createState() => new _Choice();
}

class _Choice extends State<Choice> {
  int _radioValue = 0;

  void handleRadioValueChanged(int value) {
    setState(() {
      _radioValue = value;
    });
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text("Escolha"),
      ),
      body: new Center(
        child: new Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            new Container(
              margin: EdgeInsets.only(bottom: 50.0),
              child: new Text(
                'O que você será?',
                style: new TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 30.0,
                ),
              ),
            ),
            new Row(
              children: [
                new Radio<int>(
                  groupValue: _radioValue,
                  onChanged: handleRadioValueChanged,
                  value: 0,
                ),
                new Text(
                  'Cliente     ',
                  style: new TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 20.0,
                  ),
                ),
              ],
              mainAxisAlignment: MainAxisAlignment.center,
            ),
            new Row(
              children: [
                new Radio<int>(
                  groupValue: _radioValue,
                  onChanged: handleRadioValueChanged,
                  value: 1,
                ),
                new Text(
                  'Prestador',
                  style: new TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 20.0,
                  ),
                ),
              ],
              mainAxisAlignment: MainAxisAlignment.center,
            ),
            new Container(
              height: 50.0,
              width: 150.0,
              margin: EdgeInsets.only(top: 50.0),
              child: new RaisedButton(
                child: new Text(
                  'OK!',
                  style: new TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 20.0,
                      color: Colors.white),
                ),
                onPressed: () {
                  if (_radioValue == 0) {
                    Navigator.push(
                      context,
                      new MaterialPageRoute(builder: (context) => new Posts()),
                    );
                  } else if (_radioValue == 1) {
                    Navigator.push(
                      context,
                      new MaterialPageRoute(
                          builder: (context) => new SelectServices()),
                    );
                  }
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}
