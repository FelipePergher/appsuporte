import 'package:app_suporte/Controller/ServiceController.dart';
import 'package:app_suporte/Controller/AuthController.dart';
import 'package:app_suporte/Models/Service.dart';
import 'package:app_suporte/Views/select_services.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class Services extends StatefulWidget {
  @override
  _Services createState() => new _Services();
}

class _Services extends State<Services> {
  AuthController authController = new AuthController();
  ServiceController serviceController = new ServiceController();
  final formKey = new GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text("Serviços Prestados"),
      ),
      body: new FutureBuilder<List<Service>>(
        future: serviceController.getServices(),
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            List<Service> services = snapshot.data;
            List<Service> myServices = new List<Service>();

            for (var item in services) {
              if (item.checked) {
                myServices.add(item);
              }
            }

            return new ListView.builder(
              itemBuilder: (BuildContext context, int index) => new ListView(
                      shrinkWrap: true,
                      padding: const EdgeInsets.only(
                          top: 5.0, left: 10.0, right: 10.0),
                      children: <Widget>[
                        new ListTile(
                          leading: const Icon(Icons.build, color: Colors.grey),
                          title: Text(myServices[index].name,
                              style: new TextStyle(fontSize: 18.0)),
                        ),
                      ]),
              itemCount: myServices.length,
            );
          } else if (snapshot.hasError) {
            return new Text("${snapshot.error}");
          }
          return new Center(
            child: new CircularProgressIndicator(),
          );
        },
      ),
      floatingActionButton: new FloatingActionButton(
        child: new Icon(Icons.add),
        onPressed: () {
          Navigator.push(
            context,
            new MaterialPageRoute(builder: (context) => new SelectServices()),
          );
        },
      ),
    );
  }
}
