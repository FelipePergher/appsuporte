import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ResetPassword extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text("Recuperar senha"),
      ),
      body: new Form(
        child: new ListView(children: <Widget>[
          new Container(
            margin: const EdgeInsets.only(top: 170.0),
            child: new ListTile(
              leading: const Icon(Icons.mail),
              title: new TextFormField(
                keyboardType: TextInputType.emailAddress,
                decoration: new InputDecoration(
                  hintText: "E-mail",
                ),
              ),
            ),
          ),
          new Container(
            margin: const EdgeInsets.only(top: 30.0),
            alignment: new Alignment(0.0, 0.0),
            child: new RaisedButton(
              child: new Text(
                'Enviar',
                style: new TextStyle(
                  fontSize: 20.0,
                  color: Colors.white,
                  fontWeight: FontWeight.bold,
                ),
              ),
              onPressed: () {},
            ),
          ),
        ]),
      ),
    );
  }
}
