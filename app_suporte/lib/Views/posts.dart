import 'package:app_suporte/Controller/PostController.dart';
import 'package:app_suporte/Models/Post.dart';
import 'package:app_suporte/Models/Item.dart';
import 'package:app_suporte/Code/global.dart';
import 'package:app_suporte/Views/add_post.dart';
import 'package:app_suporte/main.dart';
import 'package:app_suporte/Views/services.dart';
import 'package:app_suporte/Views/my_posts.dart';
import 'package:app_suporte/Views/select_services.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_masked_text/flutter_masked_text.dart';

class Posts extends StatefulWidget {
  @override
  _Posts createState() => new _Posts();
}

class _Posts extends State<Posts> {
  PostController postController = new PostController();
  BuildContext _scaffoldContext;
  List<Post> posts;

  void _select(Item item) {
    if (item.id == "1") {
      Navigator.push(
        context,
        new MaterialPageRoute(builder: (context) => new MyPosts()),
      );
    }
    if (item.id == "2") {
      if (Global.prestador) {
        Navigator.push(
          context,
          new MaterialPageRoute(builder: (context) => new Services()),
        );
      } else {
        Navigator.push(
          context,
          new MaterialPageRoute(builder: (context) => new SelectServices()),
        );
      }
    }
    if (item.id == "3") {
      Navigator.pushAndRemoveUntil(
          context,
          new MaterialPageRoute(
              builder: (context) => new HomePage(
                    title: "Login",
                  )),
          ModalRoute.withName('/home'));
    }
  }

  void _subscribe(Post post, String price) {
    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (_) => new Center(child: new CircularProgressIndicator()),
    );

    postController.interessePost(post.postId, post.message, price).then(
        (bool result) {
      if (result) {
        Navigator.pop(context);
        showErrorMessage("Interesse registrado com sucesso");
        setState(() {
          post.icon = new Icon(Icons.radio_button_checked, color: Colors.green);
        });
      } else {
        Navigator.pop(context);
        showErrorMessage("O interesse não pode ser registrado");
      }
    }, onError: (e) {
      Navigator.pop(context);
      showErrorMessage('Algo errado aconteceu!');
    });
  }

  void _popupSubscribe(Post post) {
    MoneyMaskedTextController precoEditingController =
        new MoneyMaskedTextController(
            decimalSeparator: ',', thousandSeparator: '.');

    TextEditingController menssageEditingController =
        new TextEditingController();

    showDialog(
        context: context,
        builder: (_) => new SimpleDialog(
              title: new Text(
                "Informe algo ao cliente",
                textAlign: TextAlign.center,
              ),
              children: <Widget>[
                new SimpleDialogOption(
                  child: TextFormField(
                    keyboardType: TextInputType.number,
                    controller: precoEditingController,
                    decoration: new InputDecoration(
                      hintText: "Preço",
                    ),
                  ),
                ),
                new SimpleDialogOption(
                  child: TextFormField(
                    controller: menssageEditingController,
                    maxLines: 3,
                    decoration: new InputDecoration(
                      hintText: "Demais observações",
                    ),
                  ),
                ),
                new Container(
                  margin:
                      const EdgeInsets.only(left: 20.0, right: 20.0, top: 10.0),
                  child: new RaisedButton(
                    onPressed: () {
                      post.message = menssageEditingController.text;
                      _subscribe(post, precoEditingController.text);
                      Navigator.pop(context);
                    },
                    child: Text(
                      "Ok",
                      style: new TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 18.0,
                        color: Colors.white,
                      ),
                    ),
                  ),
                ),
              ],
            ));
  }

  void _unsubscribe(Post post) {
    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (_) => new Center(child: new CircularProgressIndicator()),
    );

    postController.desinteressePost(post.postId).then((bool result) {
      if (result) {
        Navigator.pop(context);
        showErrorMessage("Interesse removido com sucesso");
        setState(() {
          post.icon = new Icon(Icons.radio_button_unchecked, color: Colors.red);
        });
      } else {
        Navigator.pop(context);
        showErrorMessage("O interesse não pode ser removido");
      }
    }, onError: (e) {
      Navigator.pop(context);
      showErrorMessage('Algo errado aconteceu!');
    });
  }

  void _infoInteresse(Post post) {
    showDialog(
        context: context,
        builder: (_) => new SimpleDialog(
              title: new Text(
                "Informações do Post",
                textAlign: TextAlign.center,
              ),
              children: <Widget>[
                new SimpleDialogOption(
                  child: Text('Serviço: ' + post.serviceName),
                ),
                new SimpleDialogOption(
                  child: Text('Descrição: ' + post.descricao),
                ),
                new SimpleDialogOption(
                  child: Text('Solicitante: ' + post.solicitante),
                ),
                new SimpleDialogOption(
                  child: Text(
                      'Horário Inicial: ' + post.horaInitial.substring(0, 5)),
                ),
                new SimpleDialogOption(
                  child:
                      Text('Horário Final: ' + post.horaFinal.substring(0, 5)),
                ),
                new SimpleDialogOption(
                  child: Text(
                      'Dia da Semana: ' + post.diaSemana.replaceAll('||', ',')),
                ),
                new Container(
                  margin:
                      const EdgeInsets.only(left: 20.0, right: 20.0, top: 10.0),
                  child: RaisedButton(
                    onPressed: () {
                      Navigator.pop(context);
                    },
                    child: Text(
                      "Ok",
                      style: new TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 18.0,
                        color: Colors.white,
                      ),
                    ),
                  ),
                ),
              ],
            ));
  }

  void showErrorMessage(String message) {
    Scaffold.of(_scaffoldContext).showSnackBar(new SnackBar(
          content: new Text(message),
          duration: new Duration(seconds: 5),
        ));
  }

  Widget buttonInteresse(Post post) {
    if (Global.prestador) {
      return new Row(children: <Widget>[
        new Container(
          margin: const EdgeInsets.only(right: 15.0),
          child: InkWell(
            child: post.icon,
            onTap: () {
              if (post.icon.color == Colors.red) {
                _popupSubscribe(post);
              } else {
                _unsubscribe(post);
              }
            },
          ),
        ),
      ]);
    } else {
      return new Center();
    }
  }

  List<Widget> postList(int index) {
    if (posts.length - 1 == index) {
      return <Widget>[
        new ListTile(
            leading: Icon(Icons.account_circle),
            title: new Text(posts[index].serviceName),
            subtitle: new Text(posts[index].descricao),
            trailing: buttonInteresse(posts[index]),
            onTap: () {
              _infoInteresse(posts[index]);
            }),
        new Divider(color: Colors.brown),
        new ListTile()
      ];
    } else {
      return <Widget>[
        new ListTile(
            leading: Icon(Icons.account_circle),
            title: new Text(posts[index].serviceName),
            subtitle: new Text(posts[index].descricao),
            trailing: buttonInteresse(posts[index]),
            onTap: () {
              _infoInteresse(posts[index]);
            }),
        new Divider(color: Colors.brown),
      ];
    }
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
          title: new Text("Publicações"),
          automaticallyImplyLeading: false,
          actions: <Widget>[
            new PopupMenuButton(
              onSelected: _select,
              itemBuilder: (BuildContext context) {
                return appBarOptions.map((Item option) {
                  return new PopupMenuItem<Item>(
                    value: option,
                    child: new Text(option.option),
                  );
                }).toList();
              },
            ),
            new Builder(builder: (BuildContext context) {
              _scaffoldContext = context;
              return new Center();
            }),
          ]),
      body: new FutureBuilder<List<Post>>(
        future: postController.getPosts(),
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            if (posts == null) {
              posts = snapshot.data;

              if (posts.length == 0) {
                return new Center(
                  child: new Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      new Container(
                        margin: const EdgeInsets.only(bottom: 20.0),
                        child:
                            new Icon(Icons.sentiment_dissatisfied, size: 60.0),
                      ),
                      new Text(
                        'Não existe nenhuma publicação em aberto para seus serviços!',
                        style: new TextStyle(fontSize: 20.0),
                        textAlign: TextAlign.center,
                      )
                    ],
                  ),
                );
              }

              if (Global.prestador) {
                for (var post in posts) {
                  if (post.interesse == false) {
                    post.icon = new Icon(Icons.radio_button_unchecked,
                        color: Colors.red);
                  } else if (post.interesse == true) {
                    post.icon = new Icon(Icons.radio_button_checked,
                        color: Colors.green);
                  }
                }
              }
            }

            if (posts.length == 0) {
              return new Center(
                child: new Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    new Container(
                      margin: const EdgeInsets.only(bottom: 20.0),
                      child: new Icon(Icons.sentiment_dissatisfied, size: 60.0),
                    ),
                    new Text(
                      'Não existe nenhuma publicação em aberto para seus serviços!',
                      style: new TextStyle(fontSize: 20.0),
                      textAlign: TextAlign.center,
                    )
                  ],
                ),
              );
            }

            return new ListView.builder(
              itemBuilder: (BuildContext context, int index) =>
                  new Column(children: postList(index)),
              itemCount: posts.length,
            );
          } else if (snapshot.hasError) {
            return new Text("${snapshot.error}");
          }
          return new Center(
            child: new CircularProgressIndicator(),
          );
        },
      ),
      floatingActionButton: new FloatingActionButton(
        child: new Icon(Icons.add),
        onPressed: () {
          Navigator.push(
            context,
            new MaterialPageRoute(builder: (context) => new AddPost()),
          );
        },
      ),
    );
  }
}

List<Item> appBarOptions = <Item>[
  Item(id: "1", option: 'Minhas Publicações'),
  Item(id: "2", option: 'Prestação de serviços'),
  Item(id: "3", option: 'Sair')
];
