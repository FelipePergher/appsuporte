import 'package:app_suporte/Code/global.dart';
import 'package:app_suporte/Models/Prestador.dart';
import 'package:app_suporte/Models/Service.dart';
import 'package:app_suporte/Views/posts.dart';
import 'package:flutter/material.dart';
import 'package:app_suporte/Controller/ServiceController.dart';
import 'package:app_suporte/Controller/PrestadorController.dart';

class SelectServices extends StatefulWidget {
  @override
  _SelectServices createState() => new _SelectServices();
}

class _SelectServices extends State<SelectServices> {
  ServiceController serviceController = new ServiceController();
  PrestadorController prestadorController = new PrestadorController();
  BuildContext _scaffoldContext;
  List<ServiceItem> serviceItems = new List<ServiceItem>();
  List<int> checkedServices = new List<int>();

  void showErrorMessage(String message) {
    Scaffold.of(_scaffoldContext).showSnackBar(new SnackBar(
          content: new Text(message),
          duration: new Duration(seconds: 5),
        ));
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text("Selecionar Serviços"),
      ),
      body: new FutureBuilder<List<Service>>(
        future: serviceController.getServices(),
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            List<Service> services = snapshot.data;

            if (services.length != serviceItems.length) {
              for (var item in services) {
                ServiceItem check = new ServiceItem();
                check.serviceId = item.servicoId;
                check.name = item.name;
                check.checked = item.checked;
                serviceItems.add(check);
              }
            }

            return new ListView.builder(
              itemBuilder: (BuildContext context, int index) => new ListView(
                    shrinkWrap: true,
                    padding: const EdgeInsets.only(
                        top: 5.0, left: 10.0, right: 10.0),
                    children: <Widget>[
                      Row(
                        children: <Widget>[
                          Checkbox(
                            value: serviceItems[index].checked,
                            onChanged: (bool value) {
                              setState(() {
                                serviceItems[index].checked = value;
                              });
                            },
                          ),
                          Text(serviceItems[index].name,
                              style: new TextStyle(fontSize: 18.0)),
                          new Builder(builder: (BuildContext context) {
                            _scaffoldContext = context;
                            return new Center();
                          })
                        ],
                      )
                    ],
                  ),
              itemCount: services.length,
            );
          } else if (snapshot.hasError) {
            return new Text("${snapshot.error}");
          }
          return new Center(
            child: new CircularProgressIndicator(),
          );
        },
      ),
      floatingActionButton: new FloatingActionButton(
        child: new Icon(Icons.done),
        onPressed: () {
          checkedServices.clear();
          for (var item in serviceItems) {
            if (item.checked == true) {
              checkedServices.add(item.serviceId);
            }
          }
          if (checkedServices.length >= 1) {
            showDialog(
              barrierDismissible: false,
              context: context,
              builder: (_) =>
                  new Center(child: new CircularProgressIndicator()),
            );

            prestadorController.savePrestador(Global.username).then(
                (Prestador prestador) {
              if (prestador != null) {
                Global.prestador = true;
                serviceController
                    .savePrestadorServices(
                        checkedServices, prestador.prestadorId)
                    .then((bool result) {
                  if (result) {
                    Navigator.pop(context);
                    Navigator.push(
                      context,
                      new MaterialPageRoute(builder: (context) => new Posts()),
                    );
                  }
                }, onError: (e) {
                  Navigator.pop(context);
                  showErrorMessage("Algo errado aconteceu!");
                });
              } else {
                Navigator.pop(context);
                showErrorMessage("Algo errado aconteceu!");
              }
            }, onError: (e) {
              Navigator.pop(context);
              showErrorMessage("Algo errado aconteceu!");
            });
          } else {
            showErrorMessage("Selecione pelo menos um serviço");
          }
        },
      ),
    );
  }
}

class ServiceItem {
  int serviceId;
  String name;
  bool checked = false;
}
