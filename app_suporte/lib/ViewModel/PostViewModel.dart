import 'package:app_suporte/Models/Service.dart';
import 'package:app_suporte/Models/Week.dart';
import 'package:flutter/material.dart';

class PostViewModel {
  String descricao;
  Service service;
  List<Week> weekDays;
  TimeOfDay timeInitial;
  TimeOfDay timeFinal;

  PostViewModel(
      {this.descricao,
      this.timeInitial,
      this.weekDays,
      this.service,
      this.timeFinal});
}
